package com.example.perqaratest.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.perqaratest.page.home.HomeViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import com.example.perqaratest.getValueForTest
import com.example.perqaratest.page.home.HomeUiState
import com.example.domain.Result
import com.example.domain.usecase.GetGamesListByKeyword
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import java.lang.Exception


@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    private val errorExpected = "error expected"
    private val successExpected = listOf(
        ItemUiModel(0, "coba", InfoSectionUiModel(0, "", 0.0, released = ""))
    )

    @Mock
    private lateinit var getGamesListByKeyword: GetGamesListByKeyword

    private lateinit var viewModel: HomeViewModel

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val keyword = "Test"

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = HomeViewModel(getGamesListByKeyword)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun verifyRequestGetGamesByKeywordUsingUseCase() = runTest {
        viewModel.getGamesByKeyword(keyword)

        launch(Dispatchers.IO) {
            verify(getGamesListByKeyword, times(1)).execute(keyword)
        }
    }

    @Test
    fun emitSuccessGetGamesListByKeyboard() = runTest {
        `when`(getGamesListByKeyword.execute(keyword)).thenReturn(
            flow {
                emit(Result.Success(successExpected))
            }
        )

        viewModel.getGamesByKeyword(keyword)

        launch(Dispatchers.Main) {
            assert(viewModel.uiState.getValueForTest() is HomeUiState.Success)

            val value = viewModel.uiState.getValueForTest() as HomeUiState.Success
            assertEquals(successExpected, value.data)
        }
    }

    @Test
    fun emitErrorGetGamesListByKeyboard() = runTest {
        `when`(getGamesListByKeyword.execute(keyword)).thenReturn(
            flow {
                emit(Result.Failed(Exception(errorExpected)))
            }
        )

        viewModel.getGamesByKeyword(keyword)


        launch(Dispatchers.Main) {
            assert(viewModel.uiState.getValueForTest() is HomeUiState.Error)

            val value = viewModel.uiState.getValueForTest() as HomeUiState.Error
            assertEquals(errorExpected, value.message)
        }
    }

}