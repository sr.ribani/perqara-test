package com.example.perqaratest.favorite

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.domain.Result
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import com.example.domain.usecase.GetFavoriteGames
import com.example.perqaratest.getValueForTest
import com.example.perqaratest.page.favorite.FavoriteUiState
import com.example.perqaratest.page.favorite.FavoriteViewModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class FavoriteViewModelTest {

    private val errorExpected = "error test"
    private val successExpected = listOf(
        ItemUiModel("", InfoSectionUiModel(title = "", rating = 0.0, released = ""))
    )

    @Mock
    private lateinit var useCase: GetFavoriteGames
    private lateinit var viewModel: FavoriteViewModel
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = FavoriteViewModel(useCase)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun verifyRequestGetFavoriteGamesUsingUseCase() = runTest {
        viewModel.getFavoriteGames()

        verify(useCase, times(1)).execute()
    }

    @Test
    fun emitSuccessGetFavoriteGames() = runTest {
        `when`(useCase.execute()).thenReturn(
            flow {
                emit(Result.Success(successExpected))
            }
        )

        viewModel.getFavoriteGames()

        launch(Dispatchers.Main) {
            assert(viewModel.uiState.getValueForTest() is FavoriteUiState.Success)

            val value = viewModel.uiState.getValueForTest() as FavoriteUiState.Success
            assertEquals(successExpected, value.data)
        }
    }

    @Test
    fun emitErrorGetFavoriteGames() = runTest {
        `when`(useCase.execute()).thenReturn(
            flow {
                emit(Result.Failed(Exception(errorExpected)))
            }
        )

        viewModel.getFavoriteGames()

        launch(Dispatchers.Main) {
            assert(viewModel.uiState.getValueForTest() is FavoriteUiState.Error)

            val value = viewModel.uiState.getValueForTest() as FavoriteUiState.Error
            assertEquals(errorExpected, value.message)
        }
    }
}