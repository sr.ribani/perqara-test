package com.example.perqaratest.adapter

import com.example.domain.uimodel.ItemUiModel
import com.example.perqaratest.databinding.ItemGameBinding
import com.example.uikit.base.BaseRecyclerAdapter

class GameListAdapter : BaseRecyclerAdapter<ItemUiModel, ItemGameBinding>() {

    override fun ItemGameBinding.bind(item: ItemUiModel, position: Int) {
        root.setData(item)
    }
}