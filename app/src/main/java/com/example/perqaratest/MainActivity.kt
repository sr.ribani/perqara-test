package com.example.perqaratest

import com.example.perqaratest.databinding.ActivityMainBinding
import com.example.perqaratest.page.favorite.FavoriteFragment
import com.example.perqaratest.page.home.HomeFragment
import com.example.uikit.base.BaseActivity
import com.example.uikit.base.PageStateAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val pagerAdapter by lazy {
        PageStateAdapter(this).apply {
            setItems(HomeFragment(), FavoriteFragment())
        }
    }

    override fun ActivityMainBinding.initialBinding() {
        viewPager.apply {
            adapter = pagerAdapter
            isUserInputEnabled = false
        }
        bnvMain.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home -> viewPager.currentItem = 0
                R.id.menu_favorite -> viewPager.currentItem = 1
            }
            return@setOnItemSelectedListener true
        }
    }

}