package com.example.perqaratest.page.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.onFailure
import com.example.domain.onSuccess
import com.example.domain.uimodel.ItemUiModel
import com.example.domain.usecase.GetGamesListByKeyword
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getGamesListByKeyword: GetGamesListByKeyword
) : ViewModel() {

    private val _uiState = MutableLiveData<HomeUiState>()
    val uiState: LiveData<HomeUiState> by lazy {
        _uiState
    }

    fun getGamesByKeyword(keyword: String) {
        viewModelScope.launch(Dispatchers.Main) {
            _uiState.value = HomeUiState.Loading(true)
            getGamesListByKeyword.execute(keyword).collect { result ->
                result.onSuccess {
                    _uiState.value = HomeUiState.Loading(false)
                    _uiState.value = HomeUiState.Success(it)
                }.onFailure {
                    _uiState.value = HomeUiState.Loading(false)
                    _uiState.value = HomeUiState.Error(it.message.toString())
                }
            }

        }
    }
}

sealed class HomeUiState {

    class Success(val data: List<ItemUiModel>) : HomeUiState()
    class Loading(val isLoading: Boolean) : HomeUiState()
    class Error(val message: String) : HomeUiState()

}