package com.example.perqaratest.page.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.onFailure
import com.example.domain.onSuccess
import com.example.domain.uimodel.ItemUiModel
import com.example.domain.usecase.GetFavoriteGames
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val useCase: GetFavoriteGames
) : ViewModel() {

    private val _uiState = MutableLiveData<FavoriteUiState>()
    val uiState : LiveData<FavoriteUiState> by lazy {
        _uiState
    }

    fun getFavoriteGames() = viewModelScope.launch {
        _uiState.value = FavoriteUiState.Loading(true)
        useCase.execute().collect { result ->
            result.onSuccess {
                _uiState.value = FavoriteUiState.Loading(false)
                _uiState.value = FavoriteUiState.Success(it)
            }.onFailure {
                _uiState.value = FavoriteUiState.Loading(false)
                _uiState.value = FavoriteUiState.Error(it.message.toString())
            }
        }
    }
}

sealed class FavoriteUiState {
    class Loading(val isLoading: Boolean) : FavoriteUiState()
    class Success(val data: List<ItemUiModel>) : FavoriteUiState()
    class Error(val message: String) : FavoriteUiState()
}
