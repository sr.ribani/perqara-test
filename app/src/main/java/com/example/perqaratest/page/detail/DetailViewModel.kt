package com.example.perqaratest.page.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.onFailure
import com.example.domain.onSuccess
import com.example.domain.uimodel.DetailGamesUiModel
import com.example.domain.uimodel.ItemUiModel
import com.example.domain.usecase.AddToFavorite
import com.example.domain.usecase.GetDetailGames
import com.example.domain.usecase.RemoveFromFavorite
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val getDetailGames: GetDetailGames,
    private val addToFavorite: AddToFavorite,
    private val removeFromFavorite: RemoveFromFavorite
) : ViewModel() {

    private val _uiState = MutableLiveData<DetailUiState>()
    val uiState : LiveData<DetailUiState> by lazy { _uiState }

    fun getDetailGames(id: Int) = viewModelScope.launch {
        _uiState.value = DetailUiState.Loading(true)
        getDetailGames.execute(id).collect { result ->
            result.onSuccess {
                _uiState.value = DetailUiState.Loading(false)
                _uiState.value = DetailUiState.Success(it)
            }.onFailure {
                _uiState.value = DetailUiState.Loading(false)
                _uiState.value = DetailUiState.Error(it.message.toString())
            }
        }
    }

    fun addToFavorite(item: ItemUiModel) = viewModelScope.launch {
        addToFavorite.execute(item)
    }

    fun removeFromFavorite(item: ItemUiModel) = viewModelScope.launch {
        removeFromFavorite.execute(item)
    }
}

sealed class DetailUiState {
    class Loading(val isLoading: Boolean) : DetailUiState()
    class Success(val data: DetailGamesUiModel) : DetailUiState()
    class Error(val message: String) : DetailUiState()
}