package com.example.perqaratest.page.favorite

import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.example.perqaratest.adapter.GameListAdapter
import com.example.perqaratest.databinding.FragmentFavoriteBinding
import com.example.perqaratest.page.detail.DetailGamesActivity
import com.example.uikit.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteFragment : BaseFragment<FragmentFavoriteBinding>() {

    private val viewModel : FavoriteViewModel by viewModels()

    private val adapter by lazy {
        GameListAdapter().apply {
            setOnItemClickListener { model ->
                context?.let {
                    DetailGamesActivity.start(it, model.id)
                }
            }
        }
    }

    override fun FragmentFavoriteBinding.initialBinding() {
        initObserve()
        viewModel.getFavoriteGames()

        rvMain.adapter = adapter

    }

    private fun initObserve() {
        viewModel.uiState.observe(this) { state ->
            when(state) {
                is FavoriteUiState.Error -> showMessage(state.message)
                is FavoriteUiState.Loading -> {
                    binding.apply {
                        rvMain.isVisible = !state.isLoading
                        progress.isVisible = state.isLoading
                    }
                }
                is FavoriteUiState.Success -> {
                    adapter.setItems(state.data)
                }
            }
        }
    }

}