package com.example.perqaratest.page.home

import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import com.example.perqaratest.adapter.GameListAdapter
import com.example.perqaratest.databinding.FragmentHomeBinding
import com.example.perqaratest.page.detail.DetailGamesActivity
import com.example.uikit.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val viewModel : HomeViewModel by viewModels()

    private val adapter by lazy {
        GameListAdapter().apply {
            setOnItemClickListener { model ->
                context?.let {
                    DetailGamesActivity.start(it, model.id)
                }
            }
        }
    }

    override fun FragmentHomeBinding.initialBinding() {
        initObserver()

        viewModel.getGamesByKeyword("")
        rvMain.adapter = adapter
        inputSearch.setOnTextChange {
            viewModel.getGamesByKeyword(it)
        }
    }

    private fun initObserver() {
        viewModel.uiState.observe(this) { state ->
            when(state) {
                is HomeUiState.Error -> showMessage(state.message)
                is HomeUiState.Loading -> {
                    binding.apply {
                        rvMain.isVisible = !state.isLoading
                        progress.isVisible = state.isLoading
                    }
                }
                is HomeUiState.Success -> {
                    adapter.setItems(state.data)
                }
            }
        }
    }

}