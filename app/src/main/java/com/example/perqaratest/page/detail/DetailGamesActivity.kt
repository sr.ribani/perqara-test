package com.example.perqaratest.page.detail

import android.content.Context
import android.content.Intent
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.example.domain.uimodel.DetailGamesUiModel
import com.example.perqaratest.R
import com.example.perqaratest.databinding.ActivityDetailGameBinding
import com.example.uikit.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailGamesActivity : BaseActivity<ActivityDetailGameBinding>() {

    companion object {
        private const val ID = "id"

        fun start(context: Context, id: Int) {
            val intent = Intent(context, DetailGamesActivity::class.java).apply {
                putExtra(ID, id)
            }
            context.startActivity(intent)
        }
    }

    private val viewModel: DetailViewModel by viewModels()

    private lateinit var model: DetailGamesUiModel

    override fun ActivityDetailGameBinding.initialBinding() {
        initObserver()

        val id = intent.getIntExtra(ID, 0)
        viewModel.getDetailGames(id)

        ivFavorite.setOnClickListener {
            setFavorite(!model.item.isFavorite)
        }
    }

    private fun initObserver() {
        viewModel.uiState.observe(this) { state ->
            when(state) {
                is DetailUiState.Error -> showMessage(state.message)
                is DetailUiState.Loading -> {
                    binding.apply {
                        content.isVisible = !state.isLoading
                        progress.isVisible = state.isLoading
                    }
                }
                is DetailUiState.Success -> {
                    model = state.data
                    setupView()
                }
            }
        }
    }

    private fun setupView() = with(binding) {
        Glide.with(this@DetailGamesActivity)
            .load(model.item.posterUrl)
            .into(ivPoster)

        info.setData(model.item.info)

        tvBody.text = model.description

        setFavorite(model.item.isFavorite)
    }

    private fun setFavorite(value: Boolean) = with(binding) {
        ivFavorite.setColorFilter(
            ContextCompat.getColor(this@DetailGamesActivity,
                if (value) com.example.uikit.R.color.uikit_yellow
                else com.example.uikit.R.color.uikit_grey
            )
        )

        model.item.isFavorite = value

        if (value) {
            viewModel.addToFavorite(model.item)
        } else {
            viewModel.removeFromFavorite(model.item)
        }
    }
}