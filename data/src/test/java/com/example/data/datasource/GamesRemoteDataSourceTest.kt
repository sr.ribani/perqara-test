package com.example.data.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.data.model.ItemModel
import com.example.data.model.Response
import com.example.data.service.ApiService
import com.example.domain.Result
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class GamesRemoteDataSourceTest {

    private val errorExpected = Exception::class.java
    private val successExpected = listOf<ItemModel>()

    @Mock
    private lateinit var service: ApiService

    private val keyword = "test keyword"
    private lateinit var remoteDataSource: GamesRemoteDataSource
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        remoteDataSource = GamesRemoteDataSource(service)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun verifyRequestGetGamesListByKeywordUsingService() = runTest {
        remoteDataSource.getGamesListByKeyword(keyword)

        verify(service, times(1)).getGamesListByKeyword(keyword)
    }

    @Test
    fun emitSuccessGetGamesListByKeyword() = runTest {
        `when`(service.getGamesListByKeyword(keyword)).thenReturn(
            Response(0, "", "", successExpected)
        )

        val result = remoteDataSource.getGamesListByKeyword(keyword)

        launch(Dispatchers.IO) {
            assert(result is Result.Success)

            val value = result as Result.Success
            assertEquals(successExpected, value.data)
        }
    }

    @Test
    fun emitErrorGetGamesListByKeyword() = runTest {
        `when`(service.getGamesListByKeyword(keyword)).thenReturn(
            null
        )

        val result = remoteDataSource.getGamesListByKeyword(keyword)

        launch(Dispatchers.IO) {
            assert(result is Result.Failed)

            val value = result as Result.Failed
            assertEquals(errorExpected, value.error)
        }
    }

    @Test
    fun verifyRequestGetGetFavoriteGamesUsingService() = runTest {
        remoteDataSource.getFavoriteGames()

        verify(service, times(1)).getFavorite()
    }

    @Test
    fun emitSuccessGetFavoriteGames() = runTest {
        `when`(service.getFavorite()).thenReturn(
            Response(0, "", "", successExpected)
        )

        val result = remoteDataSource.getFavoriteGames()

        launch(Dispatchers.IO) {
            assert(result is Result.Success)

            val value = result as Result.Success
            assertEquals(successExpected, value.data)
        }
    }

    @Test
    fun emitErrorGetFavoriteGames() = runTest {
        `when`(service.getFavorite()).thenReturn(
            null
        )

        val result = remoteDataSource.getFavoriteGames()

        launch(Dispatchers.IO) {
            assert(result is Result.Failed)

            val value = result as Result.Failed
            assertEquals(errorExpected, value.error)
        }
    }

}