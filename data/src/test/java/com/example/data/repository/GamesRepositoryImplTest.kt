package com.example.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.data.datasource.GamesRemoteDataSource
import com.example.data.mapper.games.GamesModelMapper
import com.example.data.model.ItemModel
import com.example.domain.Result
import com.example.domain.repository.GamesRepository
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class GamesRepositoryImplTest {

    private val errorExpected = "error expected"
    private val successExpected = listOf(
        ItemModel("", 0, "", 0.0, "")
    )

    @Mock
    private lateinit var mapper: GamesModelMapper

    @Mock
    private lateinit var remoteDataSource: GamesRemoteDataSource

    private val keyword: String = "test"
    private lateinit var repository: GamesRepository

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        repository = GamesRepositoryImpl(
            remoteDataSource,
            mapper
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun verifyRequestGetGamesByKeywordUsingRemoteDataSource() = runTest {
        repository.getGamesListByKeyword(keyword)

        verify(remoteDataSource, times(1)).getGamesListByKeyword(keyword)
    }

    @Test
    fun emitSuccessGetGamesByKeyword() = runTest {
        `when`(remoteDataSource.getGamesListByKeyword(keyword)).thenReturn(
            Result.Success(successExpected)
        )

        val result = repository.getGamesListByKeyword(keyword).first()

         launch(Dispatchers.IO) {
             assert(result is Result.Success)

             val value = result as Result.Success
             assertEquals(successExpected.map {  mapper.map(it) }, value.data)
         }
    }

    @Test
    fun emitErrorGetGamesByKeyboard() = runTest {
        `when`(remoteDataSource.getGamesListByKeyword(keyword)).thenReturn(
            Result.Failed(Exception(errorExpected))
        )

        val result = repository.getGamesListByKeyword(keyword).first()

        launch(Dispatchers.IO) {
            assert(result is Result.Failed)

            val value = result as Result.Failed
            assertEquals(errorExpected, value.error.message)
        }
    }

    @Test
    fun verifyRequestGetFavoriteGamesUsingRemoteDataSource() = runTest {
        repository.getFavoriteGames()

        verify(remoteDataSource, times(1)).getFavoriteGames()
    }

    @Test
    fun emitSuccessGetFavoriteGames() = runTest {
        `when`(remoteDataSource.getFavoriteGames()).thenReturn(
            Result.Success(successExpected)
        )

        val result = repository.getFavoriteGames().first()

        launch(Dispatchers.IO) {
            assert(result is Result.Success)

            val value = result as Result.Success
            assertEquals(successExpected.map { mapper.map(it) }, value.data)
        }
    }

    @Test
    fun emitErrorGetFavoriteGames() = runTest {
        `when`(remoteDataSource.getFavoriteGames()).thenReturn(
            Result.Failed(Exception(errorExpected))
        )

        val result = repository.getFavoriteGames().first()

        launch(Dispatchers.IO) {
            assert(result is Result.Failed)

            val value = result as Result.Failed
            assertEquals(errorExpected, value.error.message)
        }
    }

}