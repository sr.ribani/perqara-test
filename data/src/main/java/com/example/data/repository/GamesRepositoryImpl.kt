package com.example.data.repository

import com.example.data.datasource.GamesLocalDataSource
import com.example.data.datasource.GamesRemoteDataSource
import com.example.data.mapper.games.GamesEntityMapper
import com.example.data.mapper.games.GamesModelMapper
import com.example.domain.repository.GamesRepository
import com.example.domain.Result
import com.example.domain.onFailure
import com.example.domain.onSuccess
import com.example.domain.uimodel.ItemUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GamesRepositoryImpl  @Inject constructor(
    private val remoteDataSource: GamesRemoteDataSource,
    private val localDataSource: GamesLocalDataSource,
    private val modelMapper: GamesModelMapper,
    private val entityMapper: GamesEntityMapper,
) : GamesRepository {
    override suspend fun getGamesListByKeyword(keyword: String): Flow<Result<List<ItemUiModel>>> {
        val result = remoteDataSource.getGamesListByKeyword(keyword)
        return flow<Result<List<ItemUiModel>>> {
            result.onSuccess {
                emit(Result.Success(it.map { item -> modelMapper.map(item) }))
            }.onFailure {
                emit(Result.Failed(it))
            }
        }.flowOn(Dispatchers.IO)

    }

    override suspend fun getFavoriteGames(): Flow<Result<List<ItemUiModel>>> {
        val result = localDataSource.getAllGames()
        return flow<Result<List<ItemUiModel>>> {
            emit(Result.Success(result.map {
                entityMapper.map(it)
            }))
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun addToFavorite(item: ItemUiModel) {
        val entity = entityMapper.reverse(item)
        localDataSource.addToFavorite(entity)
    }

    override suspend fun removeFromFavorite(item: ItemUiModel) {
        val entity = entityMapper.reverse(item)
        localDataSource.removeFromFavorite(entity)
    }


}
