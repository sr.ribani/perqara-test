package com.example.data.repository

import com.example.data.mapper.DetailGamesModelMapper
import com.example.data.datasource.DetailGamesRemoteDataSource
import com.example.domain.repository.DetailGamesRepository
import com.example.domain.uimodel.DetailGamesUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import com.example.domain.Result
import com.example.domain.onSuccess
import com.example.domain.onFailure
import javax.inject.Inject

class DetailGamesRepositoryImpl @Inject constructor(
    private val remoteDataSource: DetailGamesRemoteDataSource,
    private val modelMapper: DetailGamesModelMapper
) : DetailGamesRepository {

    override suspend fun getDetailRepository(id: Int): Flow<Result<DetailGamesUiModel>> {
        val result = remoteDataSource.getDetailGames(id)
        return flow<Result<DetailGamesUiModel>> {
            result.onSuccess {
                emit(Result.Success(modelMapper.map(it)))
            }.onFailure {
                emit(Result.Failed(Exception(it)))
            }
        }.flowOn(Dispatchers.IO)
    }
}