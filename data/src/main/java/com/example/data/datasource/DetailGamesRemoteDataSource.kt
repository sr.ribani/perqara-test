package com.example.data.datasource

import com.example.data.model.DetailGamesModel
import com.example.data.service.ApiService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.example.domain.Result

class DetailGamesRemoteDataSource @Inject constructor(
    private val apiService: ApiService
) {
    suspend fun getDetailGames(id: Int): Result<DetailGamesModel> {
        return try {
            val response = apiService.getDetailGames(id)
            Result.Success(response)
        } catch (e: Exception) {
            Result.Failed(e)
        }
    }

}
