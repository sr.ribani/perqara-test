package com.example.data.datasource

import com.example.data.model.ItemModel
import com.example.data.service.ApiService
import com.example.domain.Result
import javax.inject.Inject
import kotlin.Exception

class GamesRemoteDataSource @Inject constructor(
    private val apiService: ApiService
) {
    suspend fun getGamesListByKeyword(keyword: String): Result<List<ItemModel>> {
        return try {
            val response = apiService.getGamesListByKeyword(keyword)
            Result.Success(response.results)
        } catch (e: Exception) {
            Result.Failed(e)
        }
    }

    suspend fun getFavoriteGames() : Result<List<ItemModel>> {
        return try {
            val response = apiService.getFavorite()
            Result.Success(response.results)
        } catch (e: Exception) {
            Result.Failed(e)
        }
    }
}