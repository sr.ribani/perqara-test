package com.example.data.datasource

import com.example.data.service.local.dao.ItemDao
import com.example.data.service.local.entity.ItemEntity
import javax.inject.Inject

class GamesLocalDataSource @Inject constructor(
    private val dao: ItemDao
) {

    suspend fun getAllGames() : List<ItemEntity> {
        return dao.getAllGames()
    }

    suspend fun addToFavorite(entity: ItemEntity) {
        dao.insert(entity)
    }

    suspend fun removeFromFavorite(entity: ItemEntity) {
        dao.delete(entity)
    }

    suspend fun isFavorite(id: Int) : Boolean {
        return dao.getGamesById(id).isNotEmpty()
    }

}