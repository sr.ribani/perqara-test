package com.example.data.model

data class DetailGamesModel(
    val added: Int,
    val background_image: String,
    val description: String,
    val id: Int,
    val name: String,
    val name_original: String,
    val rating: Double,
    val released: String,
    val updated: String
)