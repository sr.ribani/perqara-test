package com.example.data.model

data class Response<T>(
    val count: Int,
    val next: String,
    val previous: Any,
    val results: List<T>
)