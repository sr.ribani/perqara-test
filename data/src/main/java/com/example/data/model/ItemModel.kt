package com.example.data.model

data class ItemModel(
    val background_image: String,
    val id: Int,
    val name: String,
    val rating: Double,
    val released: String
)