package com.example.data.mapper.games

import com.example.data.mapper.Mapper
import com.example.data.service.local.entity.ItemEntity
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import javax.inject.Inject

class GamesEntityMapper @Inject constructor() : Mapper<ItemEntity, ItemUiModel>() {
    override fun map(model: ItemEntity): ItemUiModel {
        return ItemUiModel(
            model.id,
            model.backgroundImage,
            InfoSectionUiModel(
                title = model.name,
                released = model.released,
                rating = model.rating
            )
        )
    }

    fun reverse(model: ItemUiModel) : ItemEntity {
        return ItemEntity(
            model.posterUrl,
            model.id,
            model.info.title,
            model.info.rating,
            model.info.released
        )
    }
}