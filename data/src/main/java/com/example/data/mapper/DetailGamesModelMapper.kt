package com.example.data.mapper

import com.example.data.model.DetailGamesModel
import com.example.domain.uimodel.DetailGamesUiModel
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import javax.inject.Inject

class DetailGamesModelMapper @Inject constructor() : Mapper<DetailGamesModel, DetailGamesUiModel>(){
    override fun map(model: DetailGamesModel): DetailGamesUiModel {
        return DetailGamesUiModel(
            model.description,
            ItemUiModel(
                model.id,
                model.background_image,
                InfoSectionUiModel(
                    model.added,
                    model.name_original,
                    model.rating,
                    model.name,
                    model.released
                )
            )
        )
    }

}
