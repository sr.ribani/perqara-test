package com.example.data.mapper

abstract class Mapper<M, N> {
    abstract fun map(model: M) : N
}