package com.example.data.mapper.games

import com.example.data.mapper.Mapper
import com.example.data.model.ItemModel
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import javax.inject.Inject

class GamesModelMapper @Inject constructor(): Mapper<ItemModel, ItemUiModel>() {
    override fun map(model: ItemModel): ItemUiModel {
        return ItemUiModel(
            model.id,
            model.background_image,
            InfoSectionUiModel(
                title = model.name,
                rating = model.rating,
                released = model.released
            )
        );
    }
}