package com.example.data.di

import com.example.data.repository.DetailGamesRepositoryImpl
import com.example.data.repository.GamesRepositoryImpl
import com.example.domain.repository.DetailGamesRepository
import com.example.domain.repository.GamesRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindsGamesRepository(repository: GamesRepositoryImpl) : GamesRepository

    @Binds
    abstract fun bindsDetailGamesRepository(repository: DetailGamesRepositoryImpl) : DetailGamesRepository
}