package com.example.data.di

import com.example.core.okHttpClient
import com.example.core.retrofitClient
import com.example.data.BuildConfig
import com.example.data.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun providesHttpClient() : OkHttpClient {
        val builder = okHttpClient(BuildConfig.DEBUG)
        return builder.build()
    }

    @Provides
    @Singleton
    fun providesApiService(okHttpClient: OkHttpClient): ApiService = retrofitClient(
        okHttpClient, BuildConfig.BASE_URL
    ).create(ApiService::class.java)
}