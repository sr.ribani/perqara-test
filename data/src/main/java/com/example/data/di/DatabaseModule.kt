package com.example.data.di;

import android.content.Context
import com.example.data.service.local.AppDatabase
import com.example.data.service.local.dao.ItemDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal class DatabaseModule {

    @Provides
    fun providesAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getDatabase(context)
    }

    @Provides
    fun providesSectionDao(
        database: AppDatabase
    ) : ItemDao {
        return database.itemDao()
    }
}