package com.example.data.service

import com.example.data.BuildConfig
import com.example.data.model.DetailGamesModel
import com.example.data.model.ItemModel
import com.example.data.model.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("games?key=${BuildConfig.API_KEY}")
    suspend fun getGamesListByKeyword(
        @Query("search")
        keyword: String
    ) : Response<ItemModel>

    suspend fun getFavorite(): Response<ItemModel>

    @GET("games/{id}?key=${BuildConfig.API_KEY}")
    suspend fun getDetailGames(
        @Path("id")
        id: Int
    ): DetailGamesModel

}
