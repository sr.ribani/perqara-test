package com.example.data.service.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.data.service.local.BaseDao
import com.example.data.service.local.entity.ItemEntity

@Dao
interface ItemDao : BaseDao<ItemEntity> {

    @Query("SELECT * FROM itementity")
    suspend fun getAllGames() : List<ItemEntity>

    @Query("SELECT * FROM itementity WHERE id = :id")
    suspend fun getGamesById(id: Int) : List<ItemEntity>

}