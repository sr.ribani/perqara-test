package com.example.data.service.local

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg item: T)

    @Update
    suspend fun update(vararg items: T)

    @Delete
    suspend fun delete(vararg items: T)

}