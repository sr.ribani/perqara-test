package com.example.data.service.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ItemEntity(
    val backgroundImage: String,
    @PrimaryKey
    val id: Int,
    val name: String,
    val rating: Double,
    val released: String
)
