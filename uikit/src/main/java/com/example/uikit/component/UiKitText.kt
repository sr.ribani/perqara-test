package com.example.uikit.component

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import com.example.uikit.R
import com.example.uikit.component.UiKitText.TextType.*
import com.google.android.material.textview.MaterialTextView

class UiKitText(context: Context, attrs: AttributeSet?) : MaterialTextView(context, attrs) {

    private var textType = SUBTITLE

    init {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.UiKitText
        )

        try {
            textType = typedArray.getEnum(R.styleable.UiKitText_textType, textType)
            setFont()
        } finally {
            typedArray.recycle()
        }
    }

    private fun setFont() {
        when (textType) {
            TITLE -> {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, 28f)
                setTypeface(null, Typeface.BOLD)
            }
            SUBTITLE -> {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, 24f)
                setTypeface(null, Typeface.BOLD)
            }
            CAPTION -> {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, 22f)
                setTypeface(null, Typeface.BOLD)
            }
            BODY -> {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, 24f)
            }
        }
    }

    inline fun <reified T : Enum<T>> TypedArray.getEnum(index: Int, default: T) =
        getInt(index, -1).let {
            if (it >= 0) enumValues<T>()[it] else default
        }

    enum class TextType {
        TITLE,
        SUBTITLE,
        CAPTION,
        BODY
    }
}