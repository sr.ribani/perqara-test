package com.example.uikit.component

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.util.TypedValue
import androidx.core.view.ViewCompat
import androidx.core.view.setPadding
import androidx.core.widget.addTextChangedListener
import com.example.uikit.R
import com.example.uikit.getColorCompat
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class UiKitTextInput(context: Context, attrs: AttributeSet?) : TextInputLayout(context, attrs) {

    private var shapeModel: ShapeAppearanceModel = ShapeAppearanceModel()
    private var boxBackground: MaterialShapeDrawable = MaterialShapeDrawable(shapeModel)

    private var onTextChange: (String) -> Unit = {}

    private val editText = TextInputEditText(context).apply {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        addTextChangedListener {
            onTextChange.invoke(it.toString())
        }
    }

    init {
        addView(editText)

        boxBackgroundMode = BOX_BACKGROUND_NONE

        isHintEnabled = false
        isHintAnimationEnabled = false

        boxBackground.apply {
            shapeAppearanceModel = shapeModel
            fillColor = ColorStateList.valueOf(
                getColorCompat(context, R.color.uikit_grey)
            )
        }

        ViewCompat.setBackground(editText, boxBackground)
        editText.setPadding(editText.paddingBottom)

        invalidate()
    }

    fun setOnTextChange(onTextChange: (String) -> Unit) {
        this.onTextChange = onTextChange
    }

}