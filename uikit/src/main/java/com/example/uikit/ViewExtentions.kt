package com.example.uikit

import android.content.Context
import androidx.core.content.ContextCompat

fun getColorCompat(context: Context, color: Int) = ContextCompat.getColor(context, color)