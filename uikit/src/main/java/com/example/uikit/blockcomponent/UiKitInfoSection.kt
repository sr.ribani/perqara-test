package com.example.uikit.blockcomponent

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.uikit.R
import com.example.uikit.databinding.InfoSectionUikitBinding

class UiKitInfoSection(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    private val binding = InfoSectionUikitBinding.inflate(LayoutInflater.from(context), this)

    fun setData(model: InfoSectionUiModel) = with(binding) {
        tvGame.apply {
            isVisible = model.played != null
            text = resources.getString(R.string.played, model.played)
        }
        tvTitle.text = model.title
        tvStar.text = model.rating.toString()
        tvCompany.apply {
            isVisible = model.company != null
            text = model.company
        }
        tvReleased.text = resources.getString(R.string.released, model.released)
    }

}