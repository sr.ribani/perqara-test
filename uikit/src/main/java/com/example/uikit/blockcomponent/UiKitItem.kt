package com.example.uikit.blockcomponent

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.example.domain.uimodel.ItemUiModel
import com.example.uikit.databinding.ItemUikitBinding

class UiKitItem(context: Context, attrs: AttributeSet?) : LinearLayoutCompat(context, attrs) {

    private val binding = ItemUikitBinding.inflate(LayoutInflater.from(context), this)

    fun setData(model: ItemUiModel) = with(binding) {
        Glide.with(context)
            .load(model.posterUrl)
            .into(ivPoster)

        info.setData(model.info)

        ivFavorite.isVisible = model.isFavorite
    }

}