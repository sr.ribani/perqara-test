package com.example.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.domain.repository.GamesRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import com.example.domain.Result
import com.example.domain.onSuccess
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import java.lang.Exception

@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class GetGamesListByKeywordTest {

    private val errorExpected = "error expected"
    private val successExpected = listOf(
        ItemUiModel(0, "coba", InfoSectionUiModel(0, "", 0.0, released = ""))
    )

    @Mock
    private lateinit var repository: GamesRepository

    private val keyword: String = "test"
    private lateinit var useCase: GetGamesListByKeyword

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        useCase = GetGamesListByKeyword(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun verifyRequestGetGamesListByKeywordUsingRepository() : Unit = runBlocking {
        useCase.execute(keyword)

        launch(Dispatchers.IO) {
            verify(repository, times(1)).getGamesListByKeyword(keyword)
        }
    }

    @Test
    fun emitSuccessGetGamesListByKeyWord() : Unit = runBlocking {
        `when`(repository.getGamesListByKeyword(keyword)).thenReturn(
            flow {
                emit(Result.Success(successExpected))
            }
        )

        val result = useCase.execute(keyword).first()

        launch(Dispatchers.IO) {
            assert(result is Result.Success)

            val value = result as Result.Success
            assertEquals(successExpected, value.data)
        }

    }

    @Test
    fun emitErrorGetGamesListByKeyword() : Unit = runBlocking {
        `when`(repository.getGamesListByKeyword(keyword)).thenReturn(
            flow {
                emit(Result.Failed(Exception(errorExpected)))
            }
        )

        val result = useCase.execute(keyword).first()

        launch(Dispatchers.IO) {
            assert(result is Result.Failed)

            val value = result as Result.Failed
            assertEquals(errorExpected, value.error.message)
        }
    }



}