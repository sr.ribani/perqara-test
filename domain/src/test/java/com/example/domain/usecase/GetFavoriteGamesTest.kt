package com.example.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.domain.Result
import com.example.domain.repository.GamesRepository
import com.example.domain.uimodel.InfoSectionUiModel
import com.example.domain.uimodel.ItemUiModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@DelicateCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class GetFavoriteGamesTest {

    private val errorExpected = "error test"
    private val successExpected = listOf(
        ItemUiModel(0,"", InfoSectionUiModel(title = "", rating = 0.0, released = ""))
    )

    @Mock
    private lateinit var repository: GamesRepository

    private lateinit var useCase: GetFavoriteGames
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        useCase = GetFavoriteGames(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun verifyRequestGetFavoriteGamesUsingRepository() = runTest {
        useCase.execute()

        verify(repository, times(1)).getFavoriteGames()
    }

    @Test
    fun emitSuccessGetFavoriteGames() = runTest {
        `when`(repository.getFavoriteGames()).thenReturn(
            flow {
                emit(Result.Success(successExpected))
            }
        )

        val result = useCase.execute().first()

        launch(Dispatchers.IO) {
            assert(result is Result.Success)

            val value = result as Result.Success
            assertEquals(successExpected, value.data)
        }
    }

    @Test
    fun emitErrorGetFavoriteGames() = runTest {
        `when`(repository.getFavoriteGames()).thenReturn(
            flow {
                emit(Result.Failed(Exception(errorExpected)))
            }
        )

        val result = useCase.execute().first()

        launch(Dispatchers.IO) {
            assert(result is Result.Failed)

            val value = result as Result.Failed
            assertEquals(errorExpected, value.error.message)
        }
    }
}