package com.example.domain.usecase

import com.example.domain.repository.GamesRepository

class GetGamesListByKeyword(private val repository: GamesRepository) {
    suspend fun execute(keyword: String) = repository.getGamesListByKeyword(keyword)
}