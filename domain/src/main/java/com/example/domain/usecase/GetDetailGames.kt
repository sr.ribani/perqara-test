package com.example.domain.usecase

import com.example.domain.repository.DetailGamesRepository

class GetDetailGames (
    private val repository: DetailGamesRepository
) {
    suspend fun execute(id: Int) = repository.getDetailRepository(id)
}
