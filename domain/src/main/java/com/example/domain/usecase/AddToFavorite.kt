package com.example.domain.usecase

import com.example.domain.repository.GamesRepository
import com.example.domain.uimodel.ItemUiModel

class AddToFavorite(
    private val repository: GamesRepository
) {
    suspend fun execute(item: ItemUiModel) = repository.addToFavorite(item)

}
