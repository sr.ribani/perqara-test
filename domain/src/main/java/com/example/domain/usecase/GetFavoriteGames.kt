package com.example.domain.usecase

import com.example.domain.repository.GamesRepository

class GetFavoriteGames(
    private val repository: GamesRepository
) {

    suspend fun execute() = repository.getFavoriteGames()

}
