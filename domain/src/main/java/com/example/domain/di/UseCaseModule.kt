package com.example.domain.di

import com.example.domain.repository.DetailGamesRepository
import com.example.domain.repository.GamesRepository
import com.example.domain.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class UseCaseModule {

    @Provides
    fun providesGetGamesListByKeyword(
        repository: GamesRepository
    ) : GetGamesListByKeyword {
        return GetGamesListByKeyword(repository)
    }

    @Provides
    fun providesGetFavoriteGames(
        repository: GamesRepository
    ) : GetFavoriteGames {
        return GetFavoriteGames(repository)
    }

    @Provides
    fun providesGetDetailGames(
        repository: DetailGamesRepository
    ) : GetDetailGames {
        return GetDetailGames(repository)
    }

    @Provides
    fun providesAddToFavorite(
        repository: GamesRepository
    ) : AddToFavorite {
        return AddToFavorite(repository)
    }

    @Provides
    fun providesRemoveFromFavorite(
        repository: GamesRepository
    ) : RemoveFromFavorite {
        return RemoveFromFavorite(repository)
    }

}