package com.example.domain.uimodel

data class ItemUiModel(
    val id: Int,
    val posterUrl: String,
    val info: InfoSectionUiModel,
    var isFavorite: Boolean = false
)
