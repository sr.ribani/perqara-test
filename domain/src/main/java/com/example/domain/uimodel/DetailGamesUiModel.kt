package com.example.domain.uimodel

data class DetailGamesUiModel(
    val description : String,
    val item: ItemUiModel
)
