package com.example.domain.uimodel

data class InfoSectionUiModel(
    val played: Int? = null,
    val title: String,
    val rating: Double,
    val company: String? = null,
    val released: String
)
