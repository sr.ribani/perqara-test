package com.example.domain.repository

import com.example.domain.uimodel.ItemUiModel
import com.example.domain.Result
import kotlinx.coroutines.flow.Flow

interface GamesRepository {
    suspend fun getGamesListByKeyword(keyword: String) : Flow<Result<List<ItemUiModel>>>
    suspend fun getFavoriteGames() : Flow<Result<List<ItemUiModel>>>
    suspend fun addToFavorite(item: ItemUiModel)
    suspend fun removeFromFavorite(item: ItemUiModel)
}
