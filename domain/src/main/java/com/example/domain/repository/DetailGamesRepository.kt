package com.example.domain.repository

import com.example.domain.uimodel.DetailGamesUiModel
import kotlinx.coroutines.flow.Flow
import com.example.domain.Result

interface DetailGamesRepository {
    suspend fun getDetailRepository(id: Int): Flow<Result<DetailGamesUiModel>>

}
