package com.example.core

sealed class NetworkResponse<out T : Any, out U : Any> {
    /**
     * Success response with body
     */
    data class Success<T : Any>(val body: T) : NetworkResponse<T, Nothing>()

    /**
     * Failure response with body
     */
    data class Error(val errorMessage: String? = null, val code : Int = 0) : NetworkResponse<Nothing, Nothing>()
}